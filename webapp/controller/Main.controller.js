sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"../model/formatter",
	"./Dialog.controller"
], function(Controller, Formatter, DialogController) {
	"use strict";
	return Controller.extend("TSPlanning.controller.Main", {
		formatter: Formatter,
		onInit: function() {
			console.log("Controller Initializat");
			// this.getView().getModel("Projects").refresh(true,true);

		},
		onAfterRendering: function() {
			console.log("on after rendering");

			this.getView().getModel("Projects").read('/CMO_TSPPRJResults');

			this.getView().getModel("Tasks").read("/CMO_TSPTSK(CMO_PRJV='')/Results");

			// console.log(this.getView().getModel());
			console.log(this.getView().getModel("Projects"));
			console.log(this.getView().getModel("Tasks"));
		},
		onProjectSelect: function(oEvent) {
			console.log("Project selected!");
				// console.log(oEvent);
				// console.log(oEvent.getParameter('selectedItem'));
			var oModel = undefined;
			var item = (oEvent.getParameter('selectedItem').getBindingContext("Projects").getProperty(oEvent.getParameter('selectedItem').getBindingContext(
					"Projects")
				.getPath()));
			// console.log(item)
			this.getView().getModel("Tasks").read("/CMO_TSPTSK(CMO_PRJV='" + item.CMO_PROJE.replace(/ /g, '%20').replace(/:/g, '%3A') + "')/Results", {
				"success": function(oData) {
					console.log(oData.results);
					oModel = new sap.ui.model.json.JSONModel(oData.results);
					console.log(oModel);
				}
			});
			// this.getView().getModel("Tasks").refresh(true,true);
			console.log(oModel);
			this.getView().setModel(oModel, "Tasks");
			console.log(this.getView().getModel("Tasks"));
			console.log(this.getView().getModel("Projects"));
		},
		onAddPress: function() {
			var that = this;
			var item = new sap.ui.model.json.JSONModel();

			this.dialog = sap.ui.xmlfragment("TSPlanning.view.Add", that);
			this.dialog.open();
			this.dialog.setModel(this.getView().getModel("Projects"), "Projects");
			this.dialog.setModel(this.getView().getModel("Tasks"), "Tasks");
			this.dialog.setModel(item, "item");

		},
		onPressBack: function() {
			console.log("MainController");
			console.log(this.getView().getModel("Projects"));
			// console.log( sap.ui.getCore().getModel("ProjectList"));
			this.dialog.close();

		},
		onUpdatePress: function() {
			var that = this;
			var item = this.dialog.getModel("item");
			var model = this.getView().getModel();
			console.log(item);
			console.log(model);
			// model.attachRequestCompleted(function(){
			// 	console.log("ANA: ----- attach");
			// });
			model.attachEventOnce("requestCompleted", function(data) {
                console.log("ANA: oModel: Metadata loaded OK", data);
               model.read("/CMO_TSPIRQResults",{
			"success": function(){
				console.log("ANA: Refreshed Succesfully!");
					sap.ui.core.BusyIndicator.hide();
			}
		});
            
            }, that);
			model.setUseBatch(false);
			//sap.ui.core.BusyIndicator.show();
			model.update("/CMO_TSPIRQResults('" + item.oData.ID.replace(/ /g, '%20') + "')", item.oData, {
				"success": function() {
					console.log("success");
					//model.refresh(true, true);
					//that.onModelRefresh();
				},
				"error": function(error) {
					console.log("Fail", error);

				},
				"merge": false,
				"async": false,
				"refreshAfterChange": false

			});
			model.refresh(true,true);
				this.dialog.close();
			
		},
		
		
		
		onRefreshPress: function(oEvent) {
			this.getView().getModel().refresh(true,true);
		},
		// //Refresh after Update
		// onModelRefresh: function (){
		// this.getView().getModel().read("/CMO_TSPIRQResults",{
		// 	"success": function(){
		// 		console.log("Refreshed Succesfully!");
		// 	}
		// });
		// },
		 
		// //End function
		onCreatePress: function(oEvent) {

			var item = this.dialog.getModel("item");
			var model = this.getView().getModel();
			console.log(item);
			console.log(model);
			model.setUseBatch(false);
			model.create("/CMO_TSPIRQResults", item.oData, {
				"success": function() {
					console.log("success");

				},
				"error": function(error) {
					console.log("Fail", error);

				},
				"merge": false,
				"async": false,
				"refreshAfterChange": false

			});
			model.refresh(true, true);
			this.dialog.close();
		},
		onPressEdit: function(oEvent) {
			var that = this;
			var item = new sap.ui.model.json.JSONModel(oEvent.getSource().getBindingContext().getProperty(oEvent.getSource().getBindingContext()
				.getPath()));

			this.dialog = sap.ui.xmlfragment("TSPlanning.view.Edit", that);
			this.dialog.open();

			this.dialog.setModel(item, "item");

		}
	});
});